# Message Queue

This is a C implementation of a message queue process. ZeroMQ has no inbuilt handler for a multiple 
publisher - mulitple subscriber pattern, which is what we require for the infrastructure. However, using 
0MQ primitives, it is easy enough to implement this pattern. For this, a central process becomes the 
subscriber to all publishers, and the sole publisher to all subscribers. To avoid the "SLOW-JOINER" problem
of 0MQ, where new publishers to a pub-sub architecture may not have their message recevied by the subscribers, 
we are using PUSH-PULL on the input side (so all __publishers__ are actually __pushers__) but the subscribers
are still subscribers. This means that __message filtering__ is not implemented publisher side (as it normally
is in 0MQ) but instead on the central node - this publisher side filtering seems counterintuitive, but actually
it saves on message transmission if the publisher knows that it has no subscribers for a particular topic
of message then it just wont send those messages.

## Architecture

As stated above, this is a simple C program that has a PULL link on the input side, to which all publishers
(__pushers__) connect, and a PUB link on the other side, to which all subscribers connect.

![Layout](./img/pull-pub.png) 

## Install
To build and run 

```bash
make && ./mq <path/to/config.yaml> &
```

## Message Bridge

As the kerdos infrastructure grows, we anticipate running the trading system as a distributed system.
To facilitate this, a **secure** message bridging system must be setup to allow messages from one machine
to be shared with another without the possibility of a bad actor snooping on our traffic. 

ZeroMQ provides a secure communication layer using the CurveCP protocol ([see docs](http://curvezmq.org/)).
This protocol provides advanced forward security (messages will be difficult to decrypt even in future, unless 
the server keys are completely compromised). 

To share messages from one server to another, a bridge server and bridge client set of programs has been created.
The server starts up a secure CurveZMQ session and listens to the localhost message queue on its machine. It is
configured to only collection messages of a certain type (see config.yaml for message types - and kerdoscommon 
MessageType enum for the correct values).

### Bridge Setup

Each message bridge server and client must have the public keys of each client that wishes to connect.
To create keys, the `curve_keygen` function can be used, or alternatively the python script in `util/cert_gen.py` 
can be used.

```bash
python util/cert_gen.py  
# This outputs client and server certificates into public and private dirs
```

The public keys then must be shared in a **secure** way on the required parties (e.g. by SCP or SSH).