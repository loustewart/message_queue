CC= gcc
CFLAGS= 
CFILES=src/seqno.c src/yaml_parse.c src/arg_parse.c src/white_list.c src/port_config.c
CLIBS=-L/usr/local/lib -lzmq -lczmq

all: mkbin mq pub sub

mkbin:
	mkdir -p 'bin'

mq: src/mq.c
	$(CC) $(CFLAGS) src/mq.c $(CFILES) -o bin/mq $(CLIBS) -lyaml

brse: src/bridge_server.c
	$(CC) $(CFLAGS) src/bridge_server.c $(CFILES) -o bin/bridge_server $(CLIBS) -lyaml

brcl: src/bridge_client.c
	$(CC) $(CFLAGS) src/bridge_client.c $(CFILES) -o bin/bridge_client $(CLIBS) -lyaml

sub: test/sub.c
	$(CC) $(CFLAGS) test/sub.c -o bin/sub $(CLIBS) -lczmq

pub: test/pub.c
	$(CC) $(CFLAGS) test/pub.c -o bin/pub $(CLIBS) -lczmq

clean: 
	rm -rf bin/*