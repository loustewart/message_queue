import os
import shutil

import zmq.auth


def generate_certificates(base_dir):
    """ Generate client and server CURVE certificate files """
    keys_dir = os.path.join(base_dir, '..', '.curve')

    # Create directories for certificates, remove old content if necessary
    for d in [keys_dir]:
        if os.path.exists(d):
            shutil.rmtree(d)
        os.mkdir(d)

    # create new keys in certificates dir
    zmq.auth.create_certificates(keys_dir, "server")
    zmq.auth.create_certificates(keys_dir, "client")


if __name__ == '__main__':
    if zmq.zmq_version_info() < (4, 0):
        raise RuntimeError(f"Security is not supported in libzmq version < 4.0. libzmq version {zmq.zmq_version()}")

    generate_certificates(os.path.dirname(__file__))
