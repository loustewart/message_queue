import random

import zmq
import time
import yaml

from kerdoscommon.messaging.trading import Execution, Side, OrderType, OrderStatus
from kerdoscommon.messaging.data import PortfolioUpdate
from kerdoscommon.factory.data_message_factory import DataMessageFactory as DF
from kerdoscommon.messaging import InternalMessage


def main():
    with open("../config.yaml", mode='r', encoding='utf-8') as file:
        config = yaml.load(file)
    with open("../config2.yaml", mode='r', encoding='utf-8') as file:
        config2 = yaml.load(file)

    context = zmq.Context()
    pub = context.socket(zmq.PUSH)
    cstr = f"tcp://localhost:{config['zeromq']['pub']}"
    print(cstr)
    pub.connect(cstr)
    assert not pub.closed
    time.sleep(1)

    sub = context.socket(zmq.SUB)
    cstr = f"tcp://localhost:{config2['zeromq']['sub']}"
    print(cstr)
    sub.connect(cstr)
    assert not sub.closed
    ten_as_byte = (10).to_bytes(1, 'little', signed=False)
    sub.setsockopt(zmq.SUBSCRIBE, b'')

    time.sleep(1)

    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    try:
        while True:
            i = random.randint(1, 4)
            if i == 1:
                c = b'foobar'
                print(f"Sent foobar")
            elif i == 2:
                c = create_signal()
                print(f"Sent signal")
            elif i == 3:
                c = create_execution()
                print(f"Sent execution")
            else:
                c = create_pupdate()
                print(f"Sent pupdate")
            pub.send(c)

            try:
                sockets = dict(poller.poll(25))  # Poll for 250ms
                if sockets and sockets.get(sub) == zmq.POLLIN:
                    msg = sub.recv()
                    # self.logger.info(f"message {msg}")
                    print(InternalMessage.deserialize(msg).payload)
            except Exception as e:
                pass
            time.sleep(1)

    except KeyboardInterrupt:
        pub.close()
        sub.close()
        context.term()


def create_signal():
    return InternalMessage("asdf", "ghjk", DF.create_signal("BTCUSD", "test_BTCUSD_1", order_quantity=123.45)).serialize()

def create_execution():
    e = Execution()
    e.time_local = 12344567
    e.time_source = 23455678
    e.source_id = "asdflkjh1234"
    e.order_id = "AvDeR4"
    e.order_type = OrderType.limit
    e.client_order_id = "test_BTCUSD_1 12343456"
    e.symbol_id = "BTCUSD"
    e.side = Side.buy
    e.last_quantity = 10.11
    e.last_price = 123.11
    e.leaves_quantity = 0.0
    e.cumulative_quantity = 10.11
    e.order_status = OrderStatus.filled
    e.average_price = 123.11
    return InternalMessage("asdf", "ghj", e).serialize()

def create_pupdate():
    p = PortfolioUpdate()
    p.source_id = "bitmex"
    p.time_local = 12343456
    p.time_source = 123434567
    p.side = 1
    p.pnl_type = 1
    p.update_type = 1
    p.average_entry_price = 12.2
    p.cumulative_quantity = 1.0
    p.realised_pnl = 1.0
    p.unrealised_pnl = 0.0
    p.strategy_id = 2
    'symbol_id'
    return InternalMessage("asdf", "ghj", p).serialize()



if __name__ == "__main__":
    main()
