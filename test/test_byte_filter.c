//
// Created by louis on 23/07/19.
//
#include "../src/mq.h"
#include <stdio.h>
#include <check.h>

START_TEST(test_filter) {
    byte expected = '\n';
    char* filter = "10";
    byte out = filter_to_byte(filter);
    ck_assert(out == expected);
}
END_TEST


Suite * filter_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Filter");

    /* Core test case */
    tc_core = tcase_create("FilterCreate");

    tcase_add_test(tc_core, test_filter);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = filter_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}