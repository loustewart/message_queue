#include "../src/zhelpers.h"
#include <stdio.h>
#include <sys/time.h>

double time_diff(struct timeval x , struct timeval y);

int main (int argc, char *argv []) {
    if(argc < 2) {
        printf("Usage: sub <port>\n");
        exit(1);
    }
    //  Socket to talk to server
    printf ("Receiving messages…\n");
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    char con_str[40];
    if(argc == 2) 
        sprintf(con_str, "tcp://localhost:%s", argv[1]);
    else
        sprintf(con_str, "tcp://%s:%s", argv[2], argv[1]);
    int rc = zmq_connect (subscriber, con_str);
    assert (rc == 0);

    rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "", 0);
    assert (rc == 0);

    //  Process 100 updates
    int update_nbr;
    long total_temp = 0;
    struct timeval before , after;
    gettimeofday(&before , NULL);
    for (update_nbr = 0; update_nbr < 1000; update_nbr++) {
        char *string = s_recv (subscriber);
        printf("Received %s\n", string);
        free (string);
    }
    gettimeofday(&after , NULL);
    printf("Time for 1000 messages: %.0lf us\n", time_diff(before , after) );

    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}

double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;
 
    x_ms = (double)x.tv_sec + (double)x.tv_usec;
    y_ms = (double)y.tv_sec + (double)y.tv_usec;
 
    diff = (double)y_ms - (double)x_ms;
 
    return diff;
}
