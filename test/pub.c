#include "../src/log.h"
#include "../src/zhelpers.h"

int main (int argc, char** argv) {
    if( argc < 2 ) {
        LL_CRITICAL("Usage: pub <port>");
        exit(1);
    }
    //  Prepare our context and publisher
    void *context = zmq_ctx_new ();
    void *publisher = zmq_socket (context, ZMQ_PUSH);
    char pub_str[40];
    sprintf(pub_str, "tcp://localhost:%s", argv[1]);
    int rc = zmq_connect (publisher, pub_str);
    assert (rc == 0);

    s_sleep(1000);
    //  Initialize random number generator
    srandom ((unsigned) time (NULL));
    while (1) {
        //  Get values that will fool the boss
        int zipcode, temperature, relhumidity;
        zipcode     = randof (100000);
        temperature = randof (215) - 80;
        relhumidity = randof (50) + 10;

        //  Send message to all subscribers
        char update [20];
        sprintf (update, "%05d %d %d", zipcode, temperature, relhumidity);
        LL_LOG("Sent %s", update);
        s_send (publisher, update);
    }
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    return 0;
}