import random

import zmq
import time
import yaml


def main():
    with open("../config.yaml", mode='r', encoding='utf-8') as file:
        config = yaml.load(file)
    with open("../config2.yaml", mode='r', encoding='utf-8') as file:
        config2 = yaml.load(file)

    context = zmq.Context()
    pub = context.socket(zmq.PUSH)
    cstr = f"tcp://localhost:{config['zeromq']['pub']}"
    print(cstr)
    pub.connect(cstr)
    assert not pub.closed
    time.sleep(1)

    sub = context.socket(zmq.SUB)
    cstr = f"tcp://localhost:{config2['zeromq']['sub']}"
    print(cstr)
    sub.connect(cstr)
    assert not sub.closed
    ten_as_byte = (10).to_bytes(1, 'little', signed=False)
    sub.setsockopt(zmq.SUBSCRIBE, ten_as_byte)

    time.sleep(1)

    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    try:
        while True:
            if random.random() < 0.5:
                pub.send(b'foobar')
                c = 'foobar'
            else:
                asdf = b'asdf'
                c = ten_as_byte + asdf
                pub.send(c)
            print(f"Sent {c}")
            try:
                sockets = dict(poller.poll(25))  # Poll for 250ms
                if sockets and sockets.get(sub) == zmq.POLLIN:
                    msg = sub.recv()
                    # self.logger.info(f"message {msg}")
                    print(f"Got: {msg}")
            except Exception as e:
                pass
            time.sleep(1)

    except KeyboardInterrupt:
        pub.close()
        sub.close()
        context.term()


if __name__ == "__main__":
    main()
