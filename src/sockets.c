//
// Created by louis on 23/07/19.
//
#include <czmq.h>
#include "log.h"

/**
 * Connect a zeromq zsock object to a target specified in @con_str
 *
 * @param sock socket object being connected
 * @param con_str connection string for remote target
 * @return -1 on failure
 */
int socket_connect(zsock_t* sock, char* con_str) {
    int rc = zsock_connect(sock, "%s", con_str);
    if(rc == -1){
        LL_CRITICAL("Error: failed to connect to port %s,  %s", con_str, strerror(errno));
        return -1;
    } else {
        LL_LOG("> Connected on %s", con_str);
    }
    return rc;
}

/**
 * Bind a zeromq zsock object on a local target specified in @con_str
 *
 * @param sock socket object being bound
 * @param con_str connection string for local bind target
 * @return -1 on failure
 */
int socket_bind(zsock_t* sock, char* con_str) {
    int rc = zsock_bind(sock, "%s", con_str);
    if(rc == -1){
        LL_CRITICAL("Error: failed to bind to port %s,  %s", con_str, strerror(errno));
        return -1;
    } else {
        LL_LOG("Bound on %s", con_str);
    }
    return rc;
}
