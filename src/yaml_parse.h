#ifndef YAML_PARSE_H_INCLUDE
#define YAML_PARSE_H_INCLUDE

#include <stdio.h>
#include "string_list.h"
#include "port_config.h"

Config* read_yaml(FILE* file);
Config* get_ports(char* filepath);

static const char* PUB = "pub";
static const char* SUB = "sub";
static const char* BRIDGE_PORT = "bridge_port";
static const char* BRIDGE_HOST = "bridge_host";
static const char* WHITELIST = "whitelist";
static const char* ZEROMQ = "zeromq";
static const char* FILTERS = "filters";

#endif