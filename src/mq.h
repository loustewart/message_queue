#ifndef MQ_H_INCLUDE
#define MQ_H_INCLUDE

#include <wordexp.h>
#include <zmq.h>
#include <czmq.h>
#include "message_logger.h"

typedef unsigned long long sequence_number_t;

sequence_number_t load_seqno();
void write_seqno(sequence_number_t seq_no);
void replace_seqno(zmsg_t* message, sequence_number_t seq_no);

void sequence_to_bytes(sequence_number_t longInt, byte* byteArray);

int zmq_pull_pub(zsock_t* frontend, zsock_t* backend, sequence_number_t* seq_no, MsgLogger* log);
int zmq_sub_push(zsock_t* frontend, zsock_t* backend);
int zmq_pull_push(zsock_t* frontend, zsock_t* backend);

int socket_bind(zsock_t* sock, char* con_str);
int socket_connect(zsock_t* sock, char* str);


char* parse_string_from_args(int argc, char** argv, int i);

byte filter_to_byte(char* filter);

#endif