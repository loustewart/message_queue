#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "string_list.h"

StringList* StringList_create() {
    StringList* list = (StringList*) malloc(sizeof(StringList));
    list->data = NULL;
    list->next = NULL;
    return list;
}

StringList* StringList_add(StringList *self, char *str) {
    StringList* node = StringList_create();
    node->data = strdup(str);
    if(!self) {
        self = node;
    }
    else {
        StringList* p = self;
        while(p->next) {
            p = p->next;
        }
        p->next = node;
    }
    return self;
}

void StringList_print(StringList *self) {
    while(self != NULL) {
        printf("\t\t- \"%s\"\n", self->data);
        self = self->next;
    }
}

void StringList_free(StringList *self) {
    StringList* head = self;
    while(head) {
        StringList* tmp = head;
        head = head->next;
        free(tmp->data);
        free(tmp);
    }
}

