#include <stdio.h>
#include "mq.h"
#include "log.h"

/**
 * Read sequence number from file into a sequence_number_t pointer
 *
 * @param seq_no unsigned long long pointer to read value into
 */
sequence_number_t load_seqno() {
    FILE* file;
    sequence_number_t seq_no = 0;
    if ((file = fopen("sequence.log", "r"))) {
        fscanf (file, "%llu", &seq_no);
        LL_LOG("Loaded sequence: %llu", seq_no);
        fclose (file);
    }
    return seq_no;
}

/**
 * Write sequence number out to file for reloading next time message queue
 * starts up
 *
 * @param seq_no unsigned long long pointer, value will be writted to file
 */
void write_seqno(const sequence_number_t seq_no) {
    LL_LOG("Writing sequence number to file: %llu", seq_no);
    FILE* file = fopen("sequence.log", "w");
    fprintf(file, "%llu", seq_no);
    fclose(file);
}

/**
 * Replace the sequence number field in a ZMQ Message with the current
 * message sequence number we have. Messages are initialised with 0 as
 * the sequence number and the message queue process is responsible for
 * populating the seqno field with the correct value before broadcasting
 * the message to all subscribers.
 *
 * To replace the seqno, we start at the 14th byte of the first frame of
 * the message, and overwrite 8 bytes with the value of the seqno (ull = 8 bytes)
 *
 * @param message ZMQ message to update
 * @param seq_no ull sequence number to write to message
 */
void replace_seqno(zmsg_t* message, sequence_number_t seq_no) {
    byte bytes[8];
    sequence_to_bytes(seq_no, bytes);
    zframe_t* first = zmsg_first(message);
    byte* data = zframe_data(first);
    for(int i = 0; i < 8; i++) {
        data[i + 13] = bytes[i];
    }
}

/**
 * Convert a sequence_number_t value to a byte array. Seqno is ull = 8 bytes,
 * so we need an 8 byte array.
 *
 * @param longInt sequence number to put in byte array
 * @param byteArray the array to write the number to
 */
void sequence_to_bytes(const sequence_number_t longInt, byte* byteArray) {
    byteArray[7] = (byte)((longInt >> 56u) & 0xFFu);
    byteArray[6] = (byte)((longInt >> 48u) & 0xFFu);
    byteArray[5] = (byte)((longInt >> 40u) & 0XFFu);
    byteArray[4] = (byte)((longInt >> 32u) & 0XFFu);
    byteArray[3] = (byte)((longInt >> 24u) & 0xFFu);
    byteArray[2] = (byte)((longInt >> 16u) & 0xFFu);
    byteArray[1] = (byte)((longInt >> 8u) & 0XFFu);
    byteArray[0] = (byte)((longInt & 0XFFu));
}