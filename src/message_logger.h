//
// Created by louis on 18/04/2020.
//

#ifndef MESSAGEQUEUE_MESSAGE_LOGGER_H
#define MESSAGEQUEUE_MESSAGE_LOGGER_H

#include "timestamp.h"
#include <stdbool.h>

#define NUM_MSGS 36

typedef struct msglog {
    int msgs [NUM_MSGS];
    FILE* file;
    timestamp_t time;
} MsgLogger;

MsgLogger* MsgLogger_create(char* path);
void MsgLogger_log(MsgLogger* log, zmsg_t* message);
void MsgLogger_destroy(MsgLogger* log);

#endif //MESSAGEQUEUE_MESSAGE_LOGGER_H
