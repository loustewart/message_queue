#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "port_config.h"
#include "string_list.h"

Config* Config_create() {
    Config* ports = (Config*) malloc(sizeof(Config));
    ports->bridge_host = NULL;
    ports->whitelist = NULL;
    return ports;
}

Config* Config_default() {
    StringList* wh = StringList_create();
    wh->data = strdup("127.0.0.1");
    
    Config* ports = Config_create();
    ports->pub = 12121; 
    ports->sub = 12122; 
    ports->bridge_port =11202;
    ports->bridge_host = strdup("127.0.0.1");
    ports->whitelist = wh;
    return ports;
}

void Config_free(Config **self) {
    // Free whitelist of ips
    StringList_free((*self)->whitelist);
    free((*self)->bridge_host);
    // free self
    free(*self);
}

void Config_print(Config *self) {
    printf("port_config: \n");
    printf("\tsub: %d\n", self->sub);
    printf("\tpub: %d\n", self->pub);
    printf("\tbridge_host: \"%s\"\n", self->bridge_host);
    printf("\tbridge_port: %d\n", self->bridge_port);
    if( self->whitelist ) {
        printf("\twhitelist: \n");
        StringList_print(self->whitelist);
    }
    if( self->filters ) {
        printf("\tfilters: \n");
        StringList_print(self->filters);
    }
    printf("\n");
}