//
// Created by louis on 18/04/2020.
//
#include "log.h"

unsigned char log_run_level = LOG_LVL_LOG;

const char * log_level_strings [] = {
        "NONE", // 0
        "CRIT", // 1
        "WARN", // 2
        "NOTE", // 3
        "INFO", // 4
        "DEBG" // 5
};