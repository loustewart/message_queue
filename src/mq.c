#include <string.h>
#include <czmq.h>
#include <errno.h>
#include "mq.h"
#include "yaml_parse.h"
#include "message_logger.h"
#include "log.h"


int run_queue(Config *portconf, MsgLogger *log) {
    //  Socket facing clients
    zsock_t* frontend = zsock_new(ZMQ_PULL);

    char subscriber[20];
    sprintf(subscriber, "tcp://*:%d", portconf->pub);
    int rc = socket_bind(frontend, subscriber);
    if(rc == -1) return -1;

    //  Socket facing services
    zsock_t* backend = zsock_new(ZMQ_PUB);
    
    char publisher[20];
    sprintf(publisher, "tcp://*:%d", portconf->sub);
    
    rc = socket_bind(backend, publisher);
    if(rc == -1) return -1;

    sequence_number_t seq_no = load_seqno();

    //  Start the proxy
    rc = zmq_pull_pub (frontend, backend, &seq_no, log);
    
    // Cleanup
    zsock_destroy(&frontend);
    zsock_destroy(&backend);
    Config_free(&portconf);
    MsgLogger_destroy(log);

    write_seqno(seq_no);
    if(rc != 0) {
        printf("Error: %s\n", strerror(errno));
        return rc;
    } else 
        return 0;
}

int zmq_pull_pub(zsock_t* frontend, zsock_t* backend, sequence_number_t* seq_no, MsgLogger* log) {
    // bool first;
    zpoller_t* poller = zpoller_new (frontend, NULL);
    while(!zctx_interrupted) {
        zsock_t* active = (zsock_t*) zpoller_wait(poller, 10);
        if (active) {
            /* there's something to receive */
            zmsg_t* message = zmsg_recv(active);
            if(message) {
                (*seq_no)+=1;
                replace_seqno(message, *seq_no);
                MsgLogger_log(log, message);
                zmsg_send(&message, backend);
                zmsg_destroy(&message);
            }
        }
    }
    zpoller_destroy(&poller);
    return 0;
}

int main (int argc, char** argv) {
    char* config_file = parse_string_from_args(argc, argv, 1);
    Config* ports = get_ports(config_file);

    MsgLogger* log = NULL;
    if (argc > 2 && strcmp(argv[2], "-l") == 0) {
        char* log_file = parse_string_from_args(argc, argv, 3);
        LL_LOG("Message logging enabled: saving to %s", log_file);
        log = MsgLogger_create(log_file);
    }

    return run_queue(ports, log);
}