//
// Created by louis on 18/04/2020.
//

#include "timestamp.h"

timestamp_t get_timestamp() {
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return (timestamp_t)(tv.tv_sec) * 1000 + (timestamp_t)(tv.tv_usec) / 1000;
}