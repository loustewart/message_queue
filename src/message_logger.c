//
// Created by louis on 18/04/2020.
//
#include <czmq.h>
#include "message_logger.h"
#include "log.h"
#include <pthread.h>

MsgLogger* copy(MsgLogger* log);
void reset(MsgLogger* log, timestamp_t now);
void *log_to_file(void *log);

MsgLogger* MsgLogger_create(char* path) {
    MsgLogger* log = (MsgLogger*) malloc(sizeof(MsgLogger));
    FILE* file;
    if ((file = fopen(path, "a"))) {

    } else {
        LL_CRITICAL("Could not open %s", path);
        exit(2);
    }
    log->file = file;
    memset(log->msgs, 0, NUM_MSGS * sizeof(int));
    LL_DEBUG("Created msg logger with file %s", path);
    return log;
}

void MsgLogger_log(MsgLogger* log, zmsg_t* message) {
    if (log != NULL) {
        zframe_t *first = zmsg_first(message);
        byte *data = zframe_data(first);  // Message type is first byte.
        if (!log->time) {
            log->time = get_timestamp();
        }
        uint idx = (uint) data[0];
        if (idx < NUM_MSGS) {
            log->msgs[idx]++;
        }
        timestamp_t now = get_timestamp();
        if (now - log->time >= MINUTE) {
            MsgLogger *new = copy(log);
            pthread_t t;
            pthread_create(&t, NULL, log_to_file, (void *) new);  // Hand off current log frame to IO thread
            reset(log, now);
        }
    }
}

void MsgLogger_destroy(MsgLogger* log) {
    if (log != NULL) {
        if (fclose(log->file)) {
            LL_CRITICAL("Could not close log file properly");
        }
        free(log);
    }
}

void reset(MsgLogger* log, timestamp_t now) {
    memset(log->msgs, 0, NUM_MSGS * sizeof(int));
    log->time = now;
}

MsgLogger* copy(MsgLogger* log) {
    MsgLogger* new = (MsgLogger*) malloc(sizeof(MsgLogger));
    new->file = log->file;
    new->time = log->time;
    memcpy(new->msgs, log->msgs, NUM_MSGS * sizeof(int));
    return new;
}

void *log_to_file(void *log) {
    MsgLogger *logger = (MsgLogger*)log;
    LL_DEBUG("Logging msg count at %llu", logger->time);
    fprintf(logger->file,
            "%llu,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
            logger->time, 
            logger->msgs[0],  logger->msgs[1],  logger->msgs[2],  logger->msgs[3],  logger->msgs[4],  logger->msgs[5],
            logger->msgs[6],  logger->msgs[7],  logger->msgs[8],  logger->msgs[9],  logger->msgs[10], logger->msgs[11],
            logger->msgs[12], logger->msgs[13], logger->msgs[14], logger->msgs[15], logger->msgs[16], logger->msgs[17],
            logger->msgs[18], logger->msgs[19], logger->msgs[20], logger->msgs[21], logger->msgs[22], logger->msgs[23],
            logger->msgs[24], logger->msgs[25], logger->msgs[26], logger->msgs[27], logger->msgs[28], logger->msgs[29],
            logger->msgs[30], logger->msgs[31], logger->msgs[32], logger->msgs[33], logger->msgs[34], logger->msgs[35]
    );
    fflush(logger->file);
    free(logger);
}
