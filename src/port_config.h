#ifndef PORTS_H_INCLUDE
#define PORTS_H_INCLUDE

#include "string_list.h"

typedef struct port_config {
    int pub;
    int sub;
    int bridge_port;
    char* bridge_host;
    StringList* whitelist;
    StringList* filters;
} Config;

Config* Config_create();
Config* Config_default();
void Config_free(Config **self);
void Config_print(Config *self);

#endif