//
// Created by louis on 18/04/2020.
//

#include <sys/time.h>
#include <stdio.h>

#ifndef MESSAGEQUEUE_TIMESTAMP_H
#define MESSAGEQUEUE_TIMESTAMP_H

typedef unsigned long long timestamp_t;

#define MINUTE 60000

timestamp_t get_timestamp();

#endif //MESSAGEQUEUE_TIMESTAMP_H
