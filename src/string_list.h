#ifndef STRING_LIST_H_INCLUDE
#define STRING_LIST_H_INCLUDE

typedef struct strlist {
    char* data;
    struct strlist* next;
} StringList;

StringList* StringList_create();
StringList* StringList_add(StringList *self, char *str);
void StringList_free(StringList *self);
void StringList_print(StringList *self);

#endif