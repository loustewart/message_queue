//
// Created by louis on 23/07/19.
//
#include "mq.h"

/**
 * Convert a string representing a 1 byte integer to a byte
 * @param filter string containing a 1 byte int
 * @return byte
 */
byte filter_to_byte(char* filter) {
    char* rem;
    byte i = strtol(filter, &rem, 10);
    return (byte)i & 0xFFu;
}
