#include <czmq.h>
#include <signal.h>
#include <stdbool.h>
#include <assert.h>
#include "mq.h"
#include "yaml_parse.h"


/**
 * Setup an authentication actor with a Whitelist of allowed IPs.
 * The whitelist is given in linked list form.
 * 
 * @param auth ZMQ Auth actor thread 
 * @param whitelist Linked list of string IP addresses to allow
 */
void setup_allowed(zactor_t* auth, StringList* whitelist) {
    while(whitelist) {
        assert(zstr_sendx(auth, "ALLOW", whitelist->data,NULL) == 0);
        zsock_wait(auth);
        whitelist = whitelist->next;
    }
}

/**
 * Setup a zmq.SUB socket to subscribe to a number of messages.
 * Message filters are described as byte filters, where the empty byte means
 * GIVE ME ALL MESSAGES. 
 * 
 * Otherwise, the filters are taken from a linked list of filters given in config.
 * These filters are given in string form, where the string represents an integer 
 * filter number. We must convert the string to an int and cast to a byte.
 * 
 * @param frontend ZMQ socket to add filters to 
 * @param filters Linked list of filters 
 */
void setup_subscription(zsock_t* frontend, StringList* filters) {
    if(!filters) {
        printf("Subscribing to ALL messages");
        int rc = zmq_setsockopt(zsock_resolve(frontend), ZMQ_SUBSCRIBE, "", 0);
        assert(rc == 0 || zmq_errno() == ETERM);
        return;
    }
    printf("> Subscribed to: ");
    while(filters) {
        byte filt_b = filter_to_byte(filters->data);
        int rc = zmq_setsockopt(zsock_resolve (frontend), ZMQ_SUBSCRIBE, &filt_b, 1);
        assert(rc == 0 || zmq_errno() == ETERM);
        printf("%d ", filt_b);
        filters = filters->next;
    }
    printf("\n");
}

/**
 * Connect to the subscriber channel and bind to a pulic IP for Pusher.
 * Pusher is a server socket running on the CureveMQ protocol, for secure
 * external communication.
 * 
 * The Subscriber will connect to internal message queue and filter out 
 * certain messages (see config.yaml for message filter list). These are
 * then bridged to the Pusher, who sends them to the outer world, encrypted
 * with a secure Eliptic Curve key.
 */
int run_bridge(Config* ports, char* curve_dir) {

    // Internal facing subscriber socket.
    zsock_t* frontend = zsock_new(ZMQ_SUB);

    char subscriber[25];
    sprintf(subscriber, "tcp://localhost:%d", ports->sub);
    setup_subscription(frontend, ports->filters);
    int rc = socket_connect(frontend, subscriber);
    if( rc == -1 ) return -1;

    // External facing SECURED publisher PUSH socket.
    // Zactor thread controls the auth.
    zactor_t* auth = zactor_new(zauth, NULL);
    assert(zstr_send(auth, "VERBOSE") == 0);
    zsock_wait(auth);
    setup_allowed(auth, ports->whitelist); // Set allowed ips whitelist
    assert(zstr_sendx(auth, "CURVE", curve_dir, NULL) == 0);
    zsock_wait(auth);

    zsock_t* backend = zsock_new(ZMQ_PUSH);
    // Load server certificate from .curve store.
    char server_key_secret[50];
    sprintf(server_key_secret, "%s/server.key_secret", curve_dir);
    zcert_t* server_cert = zcert_load(server_key_secret);
    assert(server_cert);
    zcert_apply(server_cert, backend);
    zsock_set_curve_server(backend, 1);
    // Create publisher string
    char publisher[25];
    sprintf(publisher, "tcp://*:%d", ports->bridge_port);
    rc = socket_bind(backend, publisher);
    if (rc == -1) return -1;

    // Run the main bridge loop
    rc = zmq_sub_push(frontend, backend); 
    
    // Cleanup all heap state
    zsock_destroy(&backend);
    zsock_destroy(&frontend);
    zactor_destroy(&auth);
    Config_free(&ports);

    return rc;
}

/**
 * Sub-Push loop pulls messages from the subscriber channel, and 
 * publishes them on the push channel. 
 * 
 * Pusher is bound to a PUBLIC IP address and uses CurveMQ for security, 
 * so a certificate pair must be provided.
 */
int zmq_sub_push(zsock_t* frontend, zsock_t* backend) {
    zpoller_t* poller = zpoller_new(frontend, NULL);
    while(!zctx_interrupted) {
        zsock_t* active = (zsock_t*) zpoller_wait(poller, 10);
        if( active ) {
            zmsg_t* msg = zmsg_recv(active);
            if( msg ) {
                zmsg_send(&msg, backend);
                zmsg_destroy(&msg);
            }
        }
    }
    zpoller_destroy(&poller);
    return 0;
}


int main(int argc, char** argv) {
    char* filepath = parse_string_from_args(argc, argv, 1);
    char* curve_dir = parse_string_from_args(argc, argv, 2);
    assert(filepath);
    assert(curve_dir);
    Config* ports = get_ports(filepath);
    return run_bridge(ports, curve_dir);
}
