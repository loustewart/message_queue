#include "mq.h"

char* parse_string_from_args(int argc, char** argv, int i) {
    if(argc < i+1) {
        return NULL;
    }
    else {
        wordexp_t exp_result;
        wordexp(argv[i], &exp_result, 0);
        return exp_result.we_wordv[0];
    }
}