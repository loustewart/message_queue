#include <yaml.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "yaml_parse.h"
#include "string_list.h"
#include "port_config.h"


void parse_next(yaml_parser_t* parser, yaml_event_t* event) {
    if (!yaml_parser_parse(parser, event)) {
        printf("Parser error %d\n", parser->error);
        exit(EXIT_FAILURE);
    }
}

char* scalar_event_handle(yaml_parser_t* parser, yaml_event_t* event) {
    yaml_event_delete( event ); 
    parse_next( parser, event );
    if(event->type == YAML_SCALAR_EVENT)
        return (char*)event->data.scalar.value;
    else 
        return NULL;
}

StringList* whitelist_event_handle(yaml_parser_t* parser, yaml_event_t* event) {
    yaml_event_delete( event ); 
    parse_next( parser, event );  // Should be Seq Start Event
    StringList* wh = NULL;
    if ( event->type == YAML_SEQUENCE_START_EVENT ) {
        do {
            char* ip = scalar_event_handle( parser, event );
            if(ip) {
                wh = StringList_add(wh, ip);
            }
        } while( event->type != YAML_SEQUENCE_END_EVENT);
    }
    return wh;
}

void to_data(bool* inseq, yaml_parser_t* parser, yaml_event_t* event, Config* pts) {
    char* evt = (char*) event->data.scalar.value;

    if (!*inseq && !strcmp(evt, ZEROMQ)) {
        *inseq = true;
    } else if (!*inseq) {
        return;
    }
    else if ( *inseq && !strcmp(evt, SUB) ) {
        // sub field
        pts->sub = atoi( scalar_event_handle( parser, event ) );
    }
    else if ( *inseq && !strcmp(evt, PUB) ) {
        // pub field
        pts->pub = atoi( scalar_event_handle( parser, event ) );
    }
    else if ( *inseq && !strcmp(evt, BRIDGE_PORT) ) {
        // bridge port
        pts->bridge_port = atoi( scalar_event_handle( parser, event ) );
    }
    else if ( *inseq && !strcmp(evt, BRIDGE_HOST) ) {
        // bridge host ip address
        char* ip = scalar_event_handle( parser, event );
        pts->bridge_host = strdup(ip);
    }
    else if ( *inseq && !strcmp(evt, WHITELIST) ) {
        pts->whitelist = whitelist_event_handle( parser, event );
    }
    else if ( *inseq && !strcmp(evt, FILTERS ) ) {
        pts->filters = whitelist_event_handle( parser, event );
    }
}

void handle_map(bool* inseq, yaml_parser_t* parser, yaml_event_t* event) {
    if(!*inseq) {
        char* buf = scalar_event_handle(parser, event);
        if ( !strcmp(ZEROMQ, buf) )
            *inseq = true; // in zeromq map
    } 
}

void event_switch(bool* inseq, yaml_parser_t* parser, yaml_event_t* event, Config* pts, int* maps) {
    switch(event->type) { 
        case YAML_NO_EVENT:
        case YAML_STREAM_START_EVENT:
        case YAML_STREAM_END_EVENT:
        case YAML_DOCUMENT_START_EVENT:
        case YAML_DOCUMENT_END_EVENT:
        case YAML_SEQUENCE_START_EVENT:
        case YAML_SEQUENCE_END_EVENT:
            break;
        case YAML_MAPPING_START_EVENT:
            ++(*maps);
            handle_map(inseq, parser, event);
            break;
        case YAML_MAPPING_END_EVENT:    
            if(*inseq && *maps == 1) {
                *inseq = false;
            }
            --(*maps);
            break;
        /* Data */
        case YAML_ALIAS_EVENT:
            break;
            // break;
        case YAML_SCALAR_EVENT: 
            to_data(inseq, parser, event, pts);
            break;
    }
    if(event->type != YAML_STREAM_END_EVENT)
        yaml_event_delete(event);
}

Config* read_yaml(FILE* file) {
    yaml_parser_t parser;
    yaml_event_t  event;

    Config* confports = Config_create();

    /* Initialize parser */
    if(!yaml_parser_initialize(&parser))
        fputs("Failed to initialize parser!\n", stderr);
    if(file == NULL)
        fputs("Failed to open file!\n", stderr);

    /* Set input file */
    yaml_parser_set_input_file(&parser, file);

    bool inseq = false;
    int maps = 0;

    do {
        parse_next(&parser, &event);
        event_switch(&inseq, &parser, &event, confports, &maps);
    } while(event.type != YAML_STREAM_END_EVENT);

    /* Cleanup */
    yaml_event_delete(&event);
    yaml_parser_delete(&parser);

    return confports;
}

Config* get_ports(char* filepath) {
    Config* ports = Config_default();

    if(filepath == NULL)
        return ports;
    else {
        FILE* file;
        if((file = fopen(filepath, "r"))) {
            Config* conf = read_yaml(file);
            if ( conf->sub == conf->pub ) {
                conf->sub = conf->pub + 1;
            }
            fclose(file);
            Config_print(conf);
            Config_free(&ports);
            return conf;
        }
        Config_print(ports);
        return ports;
    }
}
