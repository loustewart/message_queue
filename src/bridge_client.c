#include <czmq.h>
#include <signal.h>
#include <stdbool.h>
#include <assert.h>
#include "mq.h"
#include "yaml_parse.h"

/**
 * Run bridge client by setting up an externally facing frontend and an internally facing backend
 * to bridge messages from remote server to local server.
 *
 * @param ports Config details for connections
 * @return error code
 */
int run_bridge(Config* ports, char* curve_dir) {

    // External facing PULL subscriber
    zsock_t* frontend = zsock_new(ZMQ_PULL);
    // Setup location of client certificate
    char client_key_sec[50];
    sprintf(client_key_sec, "%s/client.key_secret", curve_dir);
    zcert_t* client_cert = zcert_load(client_key_sec);
    assert(client_cert);
    zcert_apply(client_cert, frontend);
    // Load server public key
    char server_key[50];
    sprintf(server_key, "%s/server.key", curve_dir);
    zcert_t* server_pub = zcert_load(server_key);
    zsock_set_curve_serverkey(frontend, zcert_public_txt(server_pub)); // Public key as z85 string

    char subscriber[25];
    sprintf(subscriber, "tcp://%s:%d", ports->bridge_host, ports->bridge_port);
    int rc = socket_connect(frontend, subscriber);
    if( rc == -1 ) return -1;

    // Internal facing push publisher
    zsock_t* backend = zsock_new(ZMQ_PUSH);

    char publisher[25];
    sprintf(publisher, "tcp://localhost:%d", ports->pub);
    rc = socket_connect(backend, publisher);
    if( rc == -1 ) return -1;

    zmq_pull_push(frontend, backend);

    // Cleanup
    zsock_destroy(&frontend);
    zsock_destroy(&backend);
    Config_free(&ports);

    return 0;
}

/**
 * Bridge client main loop:
 * - Polls on the externally facing frontend socket, waiting for messages from remote
 * - on message received, bridge the message onto internal queue by publishing from backend
 *
 * @param frontend PULL socket connected to remote server with CURVE security
 * @param backend PUSH socket connected to unsecured internal queue
 * @return error code (should be zero)
 */
int zmq_pull_push(zsock_t* frontend, zsock_t* backend) {
    zpoller_t* poller = zpoller_new(frontend, NULL);
    while(!zctx_interrupted) {
        zsock_t* active = (zsock_t*) zpoller_wait(poller, 10);
        if( active ) {
            zmsg_t* msg = zmsg_recv(active);
            if( msg ) {
                zmsg_send(&msg, backend);
                zmsg_destroy(&msg);
            }
        }
    }
    zpoller_destroy(&poller);
    return 0;
}

int main(int argc, char** argv) {
    char* filepath = parse_string_from_args(argc, argv, 1);
    char* curve_dir = parse_string_from_args(argc, argv, 2);
    assert(filepath);
    assert(curve_dir);
    Config* ports = get_ports(filepath);
    return run_bridge(ports, curve_dir);
}
